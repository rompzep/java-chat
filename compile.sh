#!/bin/bash

# compilation
javac `find -name '*.java'`

# génération des .jar et suppression des anciens
rm -f `find -name '*.jar'`
cd serveur && jar cfe Serveur.jar Serveur *.class
cd ../client && jar cfe Client.jar GUIClient *.class
cd ..

# génération de la javadoc
mkdir -p doc && cd doc
javadoc `find ../ -name '*.java'`
cd ..

# nettoyage et autorisation d'éxécution
rm `find -name '*.class'`
mv `find -name '*.jar'` .
chmod +x *.jar