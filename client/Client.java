import java.io.*; 
import java.net.*;

public class Client
{ 
    private DataInputStream in;
    private DataOutputStream out;
    private Socket socket;
    private String pseudo;

    public boolean connexion(String pseudo, String ip, int port)
    {
        try {              
            // établit la connexion avec le serveur
            socket = new Socket(ip, port); 
            // obtient les flux d'entrée/sortie des données
            in = new DataInputStream(socket.getInputStream()); 
            out = new DataOutputStream(socket.getOutputStream());
            this.pseudo = pseudo;
            return true;

        } catch(IOException e) { 
            // e.printStackTrace();
            return false;
        }
    }

    public boolean verifierPseudo()
    {
        try {
            envoyer("<CheckPseudo>" + pseudo);
            String received = "";

            while (!received.startsWith("<Pseudo"))
                received = in.readUTF();

            if (received.equals("<PseudoFree>"))
                return true;

            deconnexion();
            return false;

        } catch(IOException e) { 
            // e.printStackTrace();
            return false;
        }
    }

    public boolean deconnexion()
    {
        try {
            out.writeUTF("<exit>"); out.flush();
            System.out.println("Fermeture de la connexion : " + socket); 
            socket.close(); 
            System.out.println("Connexion fermée");
            in.close(); 
            out.close();            
            return true;
        } catch(IOException e) { 
            // e.printStackTrace();
            return false;
        }
    }

    public String recevoir()
    {
        try {
            return in.readUTF();
        } catch(IOException e) { 
            // e.printStackTrace();
            return "<error>";
        }
    }

    public boolean envoyer(String toSend)
    {
        try {
            out.writeUTF(toSend); out.flush();
            return true;
        } catch(IOException e) { 
            // e.printStackTrace();
            return false;
        }
    }
} 