import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*; 


public class GUIClient extends JFrame {
    /* application */
    private JFrame mainFrame;
    /* conteneurs */
    private JPanel mainPanel;
    private JPanel basPanel;
    /* composants */
    private JLabel lblNom;
    private JLabel lblIP;
	private JLabel lblPort;
    private JTextField tfNom;
    private JTextField tfIP;
	private JTextField tfPort;
    private JEditorPane publicMsgs;
    private JEditorPane inputMsg;
    private JList<String> users;
    private DefaultListModel<String> listModel;
    private JButton btnConn;
   	private JButton btnSend;
    private JTabbedPane tabs;
    /* contraintes des layouts */
    private GridBagConstraints cMain;
    private GridBagConstraints cBas;

    private Client client;
    private ServerListener serverListener;


    public GUIClient() {
        mainFrame = new JFrame("Java Chat");
        mainFrame.setSize(550, 150);
        mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
     	basPanel = new JPanel();
    	basPanel.setLayout(new GridBagLayout()); 

    	lblNom = new JLabel("Nom");
    	tfNom = new JTextField("");
    	btnConn = new JButton("Connexion");
    	lblIP = new JLabel("IP");
    	tfIP = new JTextField("127.0.0.1");
    	lblPort = new JLabel("Port");
    	tfPort = new JTextField("5000");
        listModel = new DefaultListModel<String>();
        users = new JList<String>(listModel);
        publicMsgs = new JEditorPane("text/html", "");
        publicMsgs.setEditable(false);
    	inputMsg = new JEditorPane("text/html", "");
        inputMsg.setEditable(true);
        tabs = new JTabbedPane();
        tabs.add("Public", new JScrollPane(publicMsgs));
    	btnSend = new JButton("Envoyer");
    	btnConn.setEnabled(false);

	    cMain = new GridBagConstraints();
	    cBas = new GridBagConstraints();

        client = new Client();

    	/* "branchement" des listeners */
        btnConn.addActionListener(new btnConnListener());
        btnSend.addActionListener(new btnSendListener());
        tfNom.addKeyListener(new tfSettingsListener());
        tfPort.addKeyListener(new tfSettingsListener());
        tfIP.addKeyListener(new tfSettingsListener());
        users.addMouseListener(new UsersListener());
        mainFrame.addWindowListener(new mainFrameListener());

        /* placement des composants dans les layouts */
        cMain.fill = GridBagConstraints.BOTH;
        cBas.fill = GridBagConstraints.BOTH;
        cMain.insets = new Insets(10,10,10,10);
        cBas.insets = new Insets(10,10,10,10);

        cMain.gridx = cMain.gridy = 0;
        cMain.weightx = 0.10;
        cMain.gridwidth = 1;
        mainPanel.add(lblNom, cMain);

        cMain.gridx = 1; cMain.gridy = 0;
        cMain.weightx = 0.40;
        mainPanel.add(tfNom, cMain);

        cMain.gridx = 2; cMain.gridy = 0;
        cMain.gridwidth = 2;
        cMain.weightx = 0.50;
        mainPanel.add(btnConn, cMain);

        cMain.gridx = 0; cMain.gridy = 1;
        cMain.gridwidth = 1;
        cMain.weightx = 0.10;
        mainPanel.add(lblIP, cMain);        

        cMain.gridx = 1; cMain.gridy = 1;
        cMain.weightx = 0.40;
        mainPanel.add(tfIP, cMain);        

        cMain.gridx = 2; cMain.gridy = 1;
        cMain.weightx = 0.10;
        mainPanel.add(lblPort, cMain);        

        cMain.gridx = 3; cMain.gridy = 1;
        cMain.weightx = 0.40;
        mainPanel.add(tfPort, cMain);     

        cBas.gridx = 0; cBas.gridy = 0;
        cBas.gridwidth = 1;
        cBas.gridheight = 3;
        cBas.weightx = 0.25;
        cBas.weighty = 1;
        basPanel.add(new JScrollPane(users), cBas);      

        cBas.gridx = 1; cBas.gridy = 0;
        cBas.gridwidth = 3;
        cBas.gridheight = 1;
        cBas.weightx = 0.75;
        cBas.weighty = 0.75;
        basPanel.add(tabs, cBas); 

        cBas.gridx = 1; cBas.gridy = 1;
        cBas.gridwidth = 3;
        cBas.gridheight = 1;
        cBas.weightx = 0.75;
        cBas.weighty = 0.22;
        basPanel.add(new JScrollPane(inputMsg), cBas); 

        cBas.gridx = 1; cBas.gridy = 2;
        cBas.gridwidth = 3;
        cBas.gridheight = 1;
        cBas.weightx = 0.75;
        cBas.weighty = 0.03;
        basPanel.add(btnSend, cBas); 

        cMain.gridx = 0; cMain.gridy = 2;
        cMain.weightx = 1;
        cMain.weighty = 1;
        cMain.gridwidth = 4;
        
        mainFrame.add(mainPanel);
        mainFrame.setVisible(true);
    }

    public void openChatPanel()
    {
        btnConn.setText("Déconnexion");
        tfNom.setEditable(false);
        tfIP.setEditable(false);
        tfPort.setEditable(false);
        publicMsgs.setText("");
        mainPanel.add(basPanel, cMain);
        mainFrame.pack();
        mainFrame.setSize(550, 650);
    }

    public void closeChatPanel()
    {
        // supp tous les onglets sauf Public;
        for (int i=0; i<tabs.getTabCount(); i++)
            if (!tabs.getTitleAt(i).equals("Public"))
                tabs.removeTabAt(i);

        btnConn.setText("Connexion");
        tfNom.setEditable(true);
        tfIP.setEditable(true);
        tfPort.setEditable(true);
        listModel.removeAllElements();
        users.setModel(listModel);
        mainPanel.remove(basPanel);
        mainFrame.pack();
        mainFrame.setSize(550, 150);
    }

	/*
     * classe écoutant les champs de saisie de configuration de la connexion
     * TODO Explications
     */
	class tfSettingsListener implements KeyListener {
		public void keyReleased(java.awt.event.KeyEvent evt) {
            String nom = tfNom.getText();

            if (nom.contains("<") || nom.contains(">")) {
                JOptionPane.showMessageDialog(null, "le nom ne doit pas comporter de chevrons (\"<\" | \">\") !", "ERREUR", JOptionPane.INFORMATION_MESSAGE);
                tfNom.setText(nom.substring(0, nom.length()-1));
            }
            else if (!(nom.length() != 0
                && tfIP.getText().length() != 0
                && tfPort.getText().length() != 0)) {
				btnConn.setEnabled(false);
			}
            else {
                btnConn.setEnabled(true);
            }
	   	}

		public void keyPressed(java.awt.event.KeyEvent evt) {}
		public void keyTyped(java.awt.event.KeyEvent evt) {}
	}

	/*
     * classe écoutant le bouton de connexion/déconnexion
     * TODO Explications
     */
	class btnConnListener implements ActionListener {
		public void actionPerformed(java.awt.event.ActionEvent evt) {
			if (btnConn.getText() == "Connexion") {
				if (!client.connexion(tfNom.getText(), tfIP.getText(), Integer.parseInt(tfPort.getText()))) {
                    JOptionPane.showMessageDialog(null, "échec connexion : serveur hors ligne ou réglages incorrects", "ERREUR", JOptionPane.INFORMATION_MESSAGE);
                }
                else {
                    if (!client.verifierPseudo()) {
                        JOptionPane.showMessageDialog(null, "échec connexion : pseudonyme déjà utilisé !", "ERREUR", JOptionPane.INFORMATION_MESSAGE);                        
                    }
                    else {
                        openChatPanel();
                        serverListener = new ServerListener();
                        serverListener.start();
                    }
                }
	    	} 
            else {
                serverListener.stop();    
                if (!client.deconnexion())
                    JOptionPane.showMessageDialog(null, "échec déconnexion serveur", "ERREUR", JOptionPane.INFORMATION_MESSAGE);
                closeChatPanel();
	    	}
		}
	}

    // public Boolean scrollIsAtBottom(JScrollPane sp) {
    //     JScrollBar sb = sp.getVerticalScrollBar();
    //     BoundedRangeModel m = sb.getModel();
    //     if (m.getMaximum() == m.getValue() + m.getExtent())
    //         return true;
    //     return false;
    // }
    
    /*
     * thread écoutant le serveur en boucle
     * pour mettre à jour les éléments de la GUI
     */
    class ServerListener implements Runnable
    {
        private Thread thread;
        private Boolean running;

        public void start()
        {
            thread = new Thread(this);
            thread.start();
        }

        public void stop()
        {
            running = false;
        }

        @Override
        public void run()
        {
            running = true;
            String rep;

            while ((rep = client.recevoir()) != "<error>" && running == true)
            {
                if (rep.startsWith("<PublicMsg>"))
                {
                    String msg = rep.substring("<PublicMsg>".length());
                    publicMsgs.setText(publicMsgs.getText().replaceAll("</body>|</html>|", "") + msg);
                    // if (scrollIsAtBottom((JScrollPane)tabs.getComponentAt(tabs.indexOfTab("Public"))))
                    //     publicMsgs.setCaretPosition(publicMsgs.getDocument().getLength());
                }

                else if (rep.startsWith("<PrivateMsg>"))
                {
                    rep = rep.substring("<PrivateMsg>".length());
                    String from = rep.substring(rep.indexOf("<")+1, rep.indexOf(">"));
                    String msg = rep.substring(("<" + from + ">").length());
                    
                    JEditorPane editorPane;
                    int fromIndex = tabs.indexOfTab(from);

                    if (fromIndex != -1) {
                        JScrollPane scrollPane = (JScrollPane)tabs.getComponentAt(fromIndex);
                        JViewport viewport = scrollPane.getViewport();
                        editorPane = (JEditorPane)viewport.getView();
                        editorPane.setText(editorPane.getText().replaceAll("</body>|</html>|", "") + msg);
                    }
                    else {
                        editorPane = new JEditorPane("text/html", "");
                        editorPane.setEditable(false);
                        editorPane.setText(msg);
                        JScrollPane scrollPane = new JScrollPane(editorPane);
                        tabs.addTab(from, scrollPane);
                    }
                }

                else if (rep.startsWith("<ClientExit>"))
                {
                    String exit = rep.substring("<ClientExit>".length());
                    String msg = "<i>*** " + exit + " a quitté le chat ***</i><hr>";
                    publicMsgs.setText(publicMsgs.getText().replaceAll("</body>|</html>|", "") + msg);
                    listModel.removeElement(exit);
                    users.setModel(listModel);
                }

                else if (rep.startsWith("<ClientLostConnection>"))
                {
                    String lost = rep.substring("<ClientLostConnection>".length());
                    String msg = "<i>*** " + lost + " a perdu la connexion ***</i><hr>";
                    publicMsgs.setText(publicMsgs.getText().replaceAll("</body>|</html>|", "") + msg);
                    listModel.removeElement(lost);
                    users.setModel(listModel);
                }

                else if (rep.startsWith("<NewClient>"))
                {
                    String nouv = rep.substring("<NewClient>".length());
                    String msg = "<i>*** " + nouv + " a rejoint le chat ***</i><hr>";
                    publicMsgs.setText(publicMsgs.getText().replaceAll("</body>|</html>|", "") + msg);
                    listModel.addElement(nouv);
                    users.setModel(listModel);
                }

                else if (rep.startsWith("<ConnectedClient>"))
                {
                    String conn = rep.substring("<ConnectedClient>".length());
                    listModel.addElement(conn);
                    users.setModel(listModel);
                }
            }

            if (running)
                JOptionPane.showMessageDialog(null, "connexion perdue avec le serveur ! vous allez être déconnecté", "ERREUR", JOptionPane.INFORMATION_MESSAGE);
        }
    }

	/* 
     * classe écoutant le bouton envoyer
     * envoie le contenu de inputMsg au serveur avant de l'effacer
     */
	class btnSendListener implements ActionListener
    {	
        public void actionPerformed(java.awt.event.ActionEvent evt)
        {    
            String msg = "<b>" + tfNom.getText() + " : </b><br>"
                + inputMsg.getText().replaceAll("</*html>|</*head>|</*body>|\r","")
                + "<hr>";

            String dest = tabs.getTitleAt(tabs.getSelectedIndex());

            if (dest.equals("Public")) 
                msg = "<PublicMsg>" + msg;
            else
                msg = "<PrivateMsg><" + dest + ">" + msg;
            
            if (!client.envoyer(msg))
                JOptionPane.showMessageDialog(null, "échec envoie du message", "ERREUR", JOptionPane.INFORMATION_MESSAGE);
            else
                inputMsg.setText("");
		}
	}

    /*
     * classe écoutant la fenêtre
     * se déconnecte du serveur avant de quitter l'application
     */
    class mainFrameListener implements WindowListener
    {
        public void windowClosing(java.awt.event.WindowEvent e)
        {
            if (btnConn.getText() == "Déconnexion")
                System.exit((client.deconnexion() == true) ? (0) : (-1));
        
            System.exit(0);
        }
        
        public void windowActivated(java.awt.event.WindowEvent e) {}
        public void windowClosed(java.awt.event.WindowEvent e) {}
        public void windowDeactivated(java.awt.event.WindowEvent e) {}
        public void windowDeiconified(java.awt.event.WindowEvent e) {}
        public void windowIconified(java.awt.event.WindowEvent e) {}
        public void windowOpened(java.awt.event.WindowEvent e) {}
    }

    class UsersListener implements MouseListener
    {
        public void mouseClicked(MouseEvent evt)
        {
            String dest = users.getSelectedValue();

            if (!dest.equals(tfNom.getText()))
            {
                if (tabs.indexOfTab(dest) == -1)
                {
                    JEditorPane editorPane = new JEditorPane("text/html", "");
                    editorPane.setEditable(false);
                    tabs.addTab(dest, new JScrollPane(editorPane));    
                }
                tabs.setSelectedIndex(tabs.indexOfTab(dest));
            }
        }

        public void mouseEntered(MouseEvent evt) {};
        public void mouseExited(MouseEvent evt) {};
        public void mousePressed(MouseEvent evt) {};
        public void mouseReleased(MouseEvent evt) {};
    }

    public static void main(String[] args)
    {
        GUIClient win = new GUIClient();
    }
}