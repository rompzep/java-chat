import java.io.*;
import java.net.*;
import java.util.*;
import java.text.*;
import java.util.Hashtable;


/**
 * Gestion des connexions entrantes des clients.
 * On attribue on nouveau thread ClientHandler à chaque nouveau client pour réduire la charge de travail du serveur.
 */
public class Serveur
{ 
    /**
     * Codes couleurs pour les pseudonymes
     */
    private Hashtable<String, String> colorCode = new Hashtable<String, String>()
    {{
        put("gray", "#808080");
        put("black", "#000000");
        put("red", "#FF0000");
        put("maroon", "#800000");
        put("yellow", "#FFFF00");
        put("olive", "#808000");
        put("lime", "#00FF00");
        put("green", "#008000");
        put("aqua", "#00FFFF");
        put("teal", "#008080");
        put("blue", "#0000FF");
        put("navy", "#000080");
        put("fuchsia", "#FF00FF");
        put("purple", "#800080");
    }};
    /**
     * Ordre des couleurs à attribuer aux pseudonymes
     */
    private Hashtable<String, String> colorOrder = new Hashtable<String, String>()
    {{
        put("black", "navy");
        put("navy", "green");
        put("green", "red");
        put("red", "gray");
        put("gray", "blue");
        put("blue", "fuchsia");
        put("fuchsia", "lime");
        put("lime", "maroon");
        put("maroon", "aqua");
        put("aqua", "purple");
        put("purple", "yellow");
        put("yellow", "teal");
        put("teal", "olive");
        put("olive", "black");
    }};
    private int port;
    /**
     * Couleur libre courante à attribuer pour le prochain client
     */
    private String color = "black";
    /**
     * Liste des clients connectés
     */
    static ArrayList<ClientHandler> clients = new ArrayList<>();
    /**
     * Formattage de la date utilisé pour afficher des logs
     */
    static DateFormat formDate = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");

    /**
     * Constructeur par défaut, le port utilisé est 5000
     */
    public Serveur()
    {
        this.port = 5000;   
    }

    /**
     * Constructeur pour choisir un port
     * @param port le port souhaité
     */
    public Serveur(int port)
    {
        this.port = port;
    }

    /**
     * Affiche un message sous la forme d'un journal
     * @param msg le message à afficher
     */
    public void printLog(String msg)
    {
        System.out.println(formDate.format(new Date()) + "  |  SERVEUR  |  " + msg);
    }
    
    /**
     * Méthode principale qui attribue un nouveau thread à chaque nouveau client qui demande à se connecter.
     */
    public void lancer() throws IOException
    {
        printLog("port utilisé : " + port);
        ServerSocket serverSocket = new ServerSocket(port);

        while (true)  
        { 
            Socket socket = null; 

            try
            { 
                socket = serverSocket.accept(); 
                printLog("un nouveau client se connecte : " + socket); 
                
                DataInputStream in = new DataInputStream(socket.getInputStream()); 
                DataOutputStream out = new DataOutputStream(socket.getOutputStream()); 
                
                printLog("attribution d'un nouveau thread pour le client : " + socket); 
                ClientHandler clientHandler = new ClientHandler(socket, in, out, colorCode.get(color)); 
                Thread thread = new Thread(clientHandler);
                thread.start();

                color = colorOrder.get(color);
            }
            catch (Exception e)
            { 
                socket.close(); 
                e.printStackTrace(); 
            } 
        }
    }

    public static void main(String[] args) 
    {
        Serveur serveur = null;

        if (args.length == 0)
            serveur = new Serveur();
        else if (args.length == 1)
            serveur = new Serveur(Integer.parseInt(args[0]));
        else
            System.out.println("Usage: java -jar Serveur.jar <port>\nNote: <port> est optionnel");

        try {
            serveur.lancer();
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
} 

/**
 * Thread chargé d'analyser et traiter les requêtes.
 */
class ClientHandler implements Runnable
{ 
    private Socket socket;
    private DataInputStream in; 
    private DataOutputStream out; 
    private String color;
    private String pseudo;

    
    public ClientHandler(Socket socket, DataInputStream in, DataOutputStream out, String color)  
    { 
        this.socket = socket; 
        this.in = in; 
        this.out = out;
        this.pseudo = "";
        this.color = color;
    } 
  
    public void printLog(String msg)
    {
        String acteur = "";
        if (pseudo.equals("")) acteur += "PSEUDO NON VERIFIE";
        else acteur += pseudo;
        System.out.println(Serveur.formDate.format(new Date())+"  |  "+acteur+"  |  "+msg);
    }

    @Override
    public void run()  
    {
        String received;

        while (true)  
        { 
            try {
                received = in.readUTF();
                  
                if (received.startsWith("<CheckPseudo>"))
                {  
                    boolean isTaken = false;
                    String desired = received.substring("<CheckPseudo>".length());
                    printLog("demande à vérifier la disponibilité du pseudo \"" + desired + "\"");
                    
                    for (ClientHandler client : Serveur.clients) {
                        if (client.pseudo.equals(desired)) {
                            printLog("le pseudo \"" + desired + "\" est déjà utilisé !");
                            out.writeUTF("<PseudoAlreadyTaken>"); out.flush();
                            isTaken = true;
                            break;
                        }
                    }
                    
                    if (!isTaken) {
                        pseudo = desired;
                        Serveur.clients.add(this);
                        printLog("le pseudo \"" + desired + "\" est libre");
                        out.writeUTF("<PseudoFree>"); out.flush();

                        for (ClientHandler client : Serveur.clients) {
                            printLog("signalement connexion à \"" + client.pseudo + "\"");
                            client.out.writeUTF("<NewClient>" + pseudo); out.flush();
                        
                            if (client.pseudo != pseudo) {
                                printLog("récupération du client connecté \"" + client.pseudo + "\"");
                                out.writeUTF("<ConnectedClient>" + client.pseudo); out.flush();
                            }
                        }
                    }
                }

                else if (received.equals("<exit>"))
                {       
                    printLog("demande à se déconnecter");
                    Serveur.clients.remove(this);
                    if (pseudo != "") {
                        for (ClientHandler client : Serveur.clients) {
                            printLog("signalement déconnexion à \"" + client.pseudo + "\"");
                            client.out.writeUTF("<ClientExit>" + pseudo); out.flush();
                        }
                    }
                    break;
                }

                else if (received.startsWith("<PublicMsg>"))
                {
                    printLog("envoie un message public");
                    received = received.replaceAll("<b>","<b style=\"color:" + color + "\">");
                    for (ClientHandler client : Serveur.clients)
                        client.out.writeUTF(received); out.flush();
                }

                else if (received.startsWith("<PrivateMsg>"))
                {
                    received = received.substring("<PrivateMsg>".length());
                    String dest = received.substring(received.indexOf("<")+1, received.indexOf(">"));
                    printLog("envoie un message privé à \"" + dest + "\"");

                    for (ClientHandler client : Serveur.clients) {
                        if (client.pseudo.equals(dest)) {
                            out.writeUTF("<PrivateMsg><" + dest + ">" + received.substring(dest.length()+2)); out.flush();
                            client.out.writeUTF("<PrivateMsg><" + pseudo + ">" + received.substring(dest.length()+2)); out.flush();
                            break;
                        }
                    }
                }

            } catch (IOException e1) { 
                // e1.printStackTrace();
                printLog("connexion perdue");

                Serveur.clients.remove(this);
                if (pseudo != "")
                    for (ClientHandler client : Serveur.clients)
                        try {
                            client.out.writeUTF("<ClientLostConnection>" + pseudo); out.flush();
                        } catch(IOException e2) {
                            // e2.printStackTrace();
                        }
                break;
            } 
        }

        try {
            printLog("fermeture de la connexion");
            this.socket.close(); 
            this.in.close(); 
            this.out.close(); 
            printLog("connexion fermée");
        }
        catch(IOException e3) { 
            // e3.printStackTrace();
        } 
    } 
} 