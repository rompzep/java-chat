Application de chat multi-client réalisée en Java - TP de L3\
(Java, Swing, AWT, Socket, DataStream)

# Compilation
Lancez le script suivant pour compiler les sources, générer les exécutables au format jar et produire la documentation : `./compile.sh`
# Lancement
**Serveur :** Double-clic sur `Serveur.jar` ou `java -jar Serveur.jar <port>` le port est optionnel (5000 par défaut)  
**Client :** Double-clic sur `Client.jar` ou `java -jar Client.jar`

Note: vous devez lancer le serveur dans un terminal si vous souhaitez choisir un port ou visualiser les logs.
# Fonctionnalités principales
* Application multi-thread
* Messages Publics/Privés
* Contrôle de la disponibilité des pseudonymes
* Signalement d'une perte de connexion du serveur ou d'un utilisateur
* Signalement de la connexion/déconnexion d'un utilisateur
* Pseudonymes colorés
# Internet
Testée et fonctionnelle, voici la démarche à suivre pour utiliser l'application à travers Internet :

**Côté serveur :**
1. Ouvrir un port disponible sur votre routeur.
2. Lancer le serveur depuis un terminal en spécifiant ce même port.

**Côté client :**
1. Connectez-vous en spécifiant le port précédemment ouvert ainsi que l'IP publique du routeur.

Attention : Côté serveur, l'IP à spécifier dans l'application client sera celle locale !

# Améliorations possibles
* Défilement automatique vers le bas à réception de nouveaux messages (à désactiver pour la lecture d'anciens messages).
* Abandonner les flux primitifs de String pour des flux d'objets sérialisés. Les objets échangés seraient des instances d'une classe Requête.
* Sécuriser le contenu des flux. En l'état, une simple capture de trames sous Wireshark permet de lire en clair les données échangées...
* Permettre l'envoi de smileys ou fichiers.